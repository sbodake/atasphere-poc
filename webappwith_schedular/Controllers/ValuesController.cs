﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using webappwith_schedular.Scheduler;

namespace webappwith_schedular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IAtashpereScheduler atashpereScheduler;

        public ValuesController(IAtashpereScheduler scheduler)
        {
            atashpereScheduler = scheduler;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public ActionResult<bool> Post([FromBody] string dateTime)
        {
            var result = atashpereScheduler.ScheduleJobAsyn<DiscoveryJob>(new JobDetails
            {
                JobId = 1,
                JobType = JobType.Discovery,
                RepeatOptions = RepeatOptions.RepeatOnce,
                ScheduleTime = DateTime.Parse(dateTime)
            }).GetAwaiter().GetResult();

            return result;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
