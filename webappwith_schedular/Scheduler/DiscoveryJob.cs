﻿using Newtonsoft.Json;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webappwith_schedular.Scheduler
{
    public class DiscoveryJob : IAtasphereJob
    {
        public async Task Execute(IJobExecutionContext context) 
        {
            var jobDetails = JsonConvert.DeserializeObject<JobDetails>(context.JobDetail.JobDataMap.GetString("JobDetails"));
            await Console.Error.WriteLineAsync($"Job Details {jobDetails.JobType} Scheduled at {jobDetails.ScheduleTime}");
        }
    }
}
