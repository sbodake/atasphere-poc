﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webappwith_schedular.Scheduler
{
    public class SchedulerSettings
    {
        public string QuartzSchedulerInstanceName { get; set; }
        public string QuartzSchedulerInstanceId { get; set; }
        public string QuartzThreadPoolType { get; set; }
        public long QuartzThreadPoolThreadCount { get; set; }
        public string QuartzThreadPoolThreadPriority { get; set; }
        public long QuartzJobStoreMisfireThreshold { get; set; }
        public string QuartzJobStoreType { get; set; }
        public bool QuartzJobStoreUseProperties { get; set; }
        public string QuartzJobStoreDataSource { get; set; }
        public string QuartzJobStoreTablePrefix { get; set; }
        public string QuartzJobStoreLockHandlerType { get; set; }
        public string QuartzDataSourceDefaultConnectionString { get; set; }
        public string QuartzDataSourceDefaultProvider { get; set; }
        public string QuartzSerializerType { get; set; }
    }
}
