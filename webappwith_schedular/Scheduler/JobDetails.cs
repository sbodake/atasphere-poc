﻿using System;

namespace webappwith_schedular.Scheduler
{
    public class JobDetails
    {
        public int JobId { get; set; }
        public DateTime ScheduleTime { get; set; }
        public RepeatOptions RepeatOptions { get; set; }
        public JobType JobType { get; set; }
    }

    public enum RepeatOptions
    {
        RepeatForEver,
        RepeatOnce,
        RepeatCount
    }

    public enum JobType
    {
        Validation,
        Discovery
    }
}