﻿using Newtonsoft.Json;
using Quartz;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace webappwith_schedular.Scheduler
{
    public class DiscoveryJobListner : IJobListener
    {
        public string Name => "Discovery Job Listener";

        public async Task JobExecutionVetoed(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            var jobDetails = JsonConvert.DeserializeObject<JobDetails>(context.JobDetail.JobDataMap.GetString("JobDetails"));
            await Console.Error.WriteLineAsync($"JobExecutionVetoed -- Job Details {jobDetails.JobType} Scheduled at {jobDetails.ScheduleTime}");
        }

        public async Task JobToBeExecuted(IJobExecutionContext context, CancellationToken cancellationToken = default)
        {
            var jobDetails = JsonConvert.DeserializeObject<JobDetails>(context.JobDetail.JobDataMap.GetString("JobDetails"));
            await Console.Error.WriteLineAsync($"JobToBeExecuted -- Job Details {jobDetails.JobType} Scheduled at {jobDetails.ScheduleTime}");
        }

        public async Task JobWasExecuted(IJobExecutionContext context, JobExecutionException jobException, CancellationToken cancellationToken = default)
        {
            var jobDetails = JsonConvert.DeserializeObject<JobDetails>(context.JobDetail.JobDataMap.GetString("JobDetails"));
            await Console.Error.WriteLineAsync($"JobWasExecuted -- Job Details {jobDetails.JobType} Scheduled at {jobDetails.ScheduleTime}");
        }
    }
}
