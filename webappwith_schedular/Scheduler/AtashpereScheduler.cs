﻿using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Quartz;
using Quartz.Impl;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading.Tasks;

namespace webappwith_schedular.Scheduler
{
    public class AtashpereScheduler : IAtashpereScheduler
    {
        private static IOptions<SchedulerSettings> schedulerConfig;
        private static IScheduler sched;

        public AtashpereScheduler(IOptions<SchedulerSettings> config)
        {
            schedulerConfig = config;
            StartScheduler().GetAwaiter().GetResult();
        }

        private static async Task StartScheduler()
        {
            // construct a scheduler factory
            var props = new NameValueCollection
            {
                { "quartz.scheduler.instanceName", schedulerConfig.Value.QuartzSchedulerInstanceName },
                { "quartz.scheduler.instanceId", schedulerConfig.Value.QuartzSchedulerInstanceId },
                { "quartz.threadPool.type", schedulerConfig.Value.QuartzThreadPoolType },
                { "quartz.threadPool.threadCount", schedulerConfig.Value.QuartzThreadPoolThreadCount.ToString() },
                { "quartz.threadPool.threadPriority", schedulerConfig.Value.QuartzThreadPoolThreadPriority },
                { "quartz.jobStore.misfireThreshold", schedulerConfig.Value.QuartzJobStoreMisfireThreshold.ToString() },
                { "quartz.jobStore.type", schedulerConfig.Value.QuartzJobStoreType },
                { "quartz.jobStore.useProperties", schedulerConfig.Value.QuartzJobStoreUseProperties.ToString() },
                { "quartz.jobStore.dataSource", schedulerConfig.Value.QuartzJobStoreDataSource },
                { "quartz.jobStore.tablePrefix", schedulerConfig.Value.QuartzJobStoreTablePrefix },
                { "quartz.jobStore.lockHandler.type", schedulerConfig.Value.QuartzJobStoreLockHandlerType },
                { "quartz.dataSource.default.connectionString", schedulerConfig.Value.QuartzDataSourceDefaultConnectionString },
                { "quartz.dataSource.default.provider", schedulerConfig.Value.QuartzDataSourceDefaultProvider },
                { "quartz.serializer.type", schedulerConfig.Value.QuartzSerializerType }
            };

            StdSchedulerFactory factory = new StdSchedulerFactory(props);

            // get a scheduler
            sched = await factory.GetScheduler();
            sched.ListenerManager.AddJobListener(new DiscoveryJobListner());
            await sched.Start();
        }

        public async Task<bool> ScheduleJobAsyn<T>(JobDetails atasphereJob) where T : IAtasphereJob
        {
            try
            {
                // Define the Job
                IJobDetail job = JobBuilder.Create<T>()
                    .WithIdentity(atasphereJob.JobType.ToString())
                    .UsingJobData("JobDetails", JsonConvert.SerializeObject(atasphereJob))
                    .Build();

                // Trigger the Job
                ITrigger trigger = TriggerBuilder.Create()
                    .WithSimpleSchedule(sch =>
                    {
                        // This will execute pending triggers which were left due to scheduler was unavailable
                        sch.WithMisfireHandlingInstructionNowWithExistingCount();
                    })
                    .StartAt(atasphereJob.ScheduleTime)
                    .ForJob(job)
                    .Build();

                await sched.ScheduleJob(job, trigger);
                return true;
            }
            catch (System.Exception)
            {
                return false;
            }
        }
    }
}
