﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webappwith_schedular.Scheduler
{
    public interface IAtashpereScheduler
    {
        Task<bool> ScheduleJobAsyn<T>(JobDetails atasphereJob) where T : IAtasphereJob;
    }
}
